ActiveAdmin.register Order do

controller do
  def permitted_params
    params.permit!
  end
end

  menu :priority => 3
  config.batch_actions = true

  filter :total_price
  filter :checked_out_at

  scope :all, :default => true
  scope :in_progress
  scope :complete

  index do
    column("Order", :sortable => :id) {|order| link_to "##{order.id} ", admin_order_path(order) }
    column("State")                   {|order| status_tag(order.state) }
    column("Date", :checked_out_at)
    column("Customer", :to_s)
    column("Total")                   {|order| number_to_currency order.total_price }
  end

  show do
    panel "Invoice" do

      table_for(order.line_items) do |t|
        t.column("Service") {|item| auto_link item.service        }
        t.column("Price")   {|item| number_to_currency item.price }
        tr :class => "odd" do
          td "Total:", :style => "text-align: right;"
          td number_to_currency(order.total_price)
        end
      end
    end
  end


  sidebar :customer_information, :only => :show do
    attributes_table_for order.customer do
      row("Customer") { auto_link order.customer }
      row :to_s
      row :email
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Create a new Order here" do
      f.input :customer, :collection => Customer.all.map {|customer| [customer.to_s] }
      f.input :checked_out_at, :collection => Order.all.map {|order| [order.checked_out_at] }
      f.input :total_price, :collection => Order.all.map {|order| [order.total_price] }
    end
    f.actions
  end



end
