ActiveAdmin.register Service do


  permit_params :name, :desc, :price

  scope :all, :default => true

  show :name => :name

  sidebar :service_stats, :only => :show do
    attributes_table_for resource do
      row("Total Sold")  { Order.find_with_service(resource).count }
      row("Dollar Value"){ number_to_currency LineItem.where(:service_id => resource.id).sum(:price) }
    end
  end

  sidebar :recent_orders, :only => :show do
    Order.find_with_service(resource).limit(5).collect do |order|
      auto_link(order)
    end.join(content_tag("br")).html_safe
  end



end
