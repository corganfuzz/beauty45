ActiveAdmin.register LineItem do

  permit_params :service_id, :price, :order_id, roles: []

end
