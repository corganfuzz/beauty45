class Customer < ActiveRecord::Base
  has_many :orders, :dependent => :destroy

  def to_s
    "#{fname} #{lname}"
  end

end