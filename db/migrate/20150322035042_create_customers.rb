class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname
      t.string :lname
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :sex
      t.integer :age
      t.string :email

      t.timestamps
    end
  end
end
