class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :order_id
      t.integer :service_id
      t.decimal :price

      t.timestamps
    end
  end
end
